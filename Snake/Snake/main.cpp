﻿#include<iostream>
#include<vector>
#include<easyx.h>
#include<ctime>
using namespace std;
class Sprite
{
public:
	Sprite() :Sprite(0, 0) {};//委托构造函数，调用哪一个数据都会初始化
	Sprite(int x, int y) :m_x(x), m_y(y),m_color(RED) {};
	//绘制
	virtual void  draw()
	{
		//设置颜色
		setfillcolor(m_color);

		fillrectangle(m_x, m_y,m_x+10,m_y+10);
	}

	//移动
	void moveBy(int dx, int dy)
	{
		m_x += dx;
		m_y += dy;
	}
	//碰撞检测
	bool collision(const Sprite& other)
	{
		
		return m_x == other.m_x &&  m_y == other.m_y;
	}
protected:
	int m_x;
	int m_y;
	COLORREF m_color;//颜色
};

class snake :public Sprite
{
public:
	snake() :snake(0, 0) {};
	snake(int x, int y) :Sprite(x, y),dir(VK_RIGHT) {
	      //初始化三节🐍
		nodes.push_back(Sprite(20, 0));
		nodes.push_back(Sprite(10, 0));
		nodes.push_back(Sprite(0, 0));
	};
	bool collision(const Sprite& other)
	{
		return nodes[0].collision(other);

	}

	void draw() override//重写父类的函数
	{
		for (auto x : nodes)
		{
			x.draw();
		}

	}

	void bodymove()
	{
		for (auto i = nodes.size() - 1; i > 0; i--)
		{
			nodes[i] = nodes[i - 1];
	   }
		switch (dir)
		{
		case VK_UP:
			nodes[0].moveBy(0, -10);
			break;
		case VK_DOWN:
			nodes[0].moveBy(0,10);
			break;
		case VK_LEFT:
			nodes[0].moveBy(-10, 0);
			break;
		case VK_RIGHT:
			nodes[0].moveBy(10, 0);
			break;
		}
	}
	//🐍增加一节
	void incrment()
	{
		nodes.push_back(Sprite());
	}
private:
	std::vector<Sprite>nodes;
public:
	int dir;//🐍的方向
};

class food :public Sprite
{
public: 
	food():Sprite(0,0) {
	//随机生成坐标
		m_x = rand()%64*10;//保证食物的坐标一定是10的整数倍
		m_y = rand()%48*10;
	}
	  void draw()override
	  {
		  setfillcolor(m_color);
		  solidellipse(m_x, m_y, m_x + 10, m_y + 10);

	   }
	  void changePos()
	  {
		  m_x = rand() % 64 * 10;//保证食物的坐标一定是10的整数倍
		  m_y = rand() % 48 * 10;
	  }
};

//游戏场景
class GameScene
{
public:
	GameScene() {
	
	};

	void run()
	{
		BeginBatchDraw();//双缓冲绘图
		cleardevice();
		snake.draw();
		food.draw();
		EndBatchDraw();
		//移动🐍，就是改变🐍的坐标
		snake.bodymove();
		//吃食物
		snakeEatFood();
		//获取消息
		ExMessage msg = { 0 };
		while (peekmessage(&msg, EX_KEY))
		{
			onMsg(msg);

		}
	}
	//响应消息，鼠标消息，键盘消息
	void onMsg(const ExMessage& msg)
	{
		//如果有键盘消息（有没有按键按下）
		if (msg.message == WM_KEYDOWN)
		{
			//判断具体是哪一个按键按下
			snake.dir = msg.vkcode;
		}
	}

	//判断🐍能否吃到食物
	void snakeEatFood()
	{
		if (snake.collision(food))
		{
			//蛇的节数增加
			snake.incrment();
			//食物重新产生在别的位置

			food.changePos();
		}

	}
private:
	snake snake;
	food food;
};



int main()
{
	initgraph(640, 480);
	//设置随机数种子
	srand(time(nullptr));
	GameScene scene;
	while (1)
	{
		scene.run();
		Sleep(50);
	}
	system("pause");
	return 0;
}